SpeechSynthesizer voice;//语音合成对象
        public MainPage()
        {
            InitializeComponent();
            this.voice = new SpeechSynthesizer();
        }
        private async void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (textBox1.Text!="")
                {
                    button1.IsEnabled = false;
                    await voice.SpeakTextAsync(textBox1.Text); //文本语音合成
                    button1.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show("请输入要读取的内容");
                }
            }
            catch (Exception ex)
            {
                erro.Text = ex.ToString();
            }
        }